<?php

namespace App\Services;

use Symfony\Component\HttpClient\HttpClient;

class MoverApi {

    public $httpClient;
    public $connParams;
    public $username;
    public $password;
    public $serverUrl;
    public $authToken;

    public function __construct(string $username, string $password, bool $test = true)
    {
        $mainServer = "https://api.mover24.ru";
        $testServer = "https://test.mover24.ru";

        $this->username = $username;
        $this->password = $password;
        $this->serverUrl = $test ? $testServer : $mainServer;
        $this->connParams = $test ? ['headers' => ['cookie' => 'movertest=supermover']] : [];
        /* @var Symfony\Component\HttpClient\HttpClient $httpClient */
        $this->httpClient = HttpClient::create($this->connParams);
    }

    public function auth(){
        try {
            $response = $this->httpClient->request('POST', $this->serverUrl.'/login/device.json',
                [ 'body' => ['username' => $this->username, 'password' => $this->password] ]
            );

            if(200 == $response->getStatusCode()){
                $matches = []; preg_match('/token=(.[^;\s]+)/', $response->getHeaders()['set-cookie'][0], $matches);
                if(\array_key_exists(1, $matches)){
                    $this->authToken = $matches[1];
                }
            }
        } catch (\Exception $e) {
            $error = 'Поймано исключение: '.  $e->getMessage(). "\n";
        }
        return $response;
    }

    public function getSchemas(){
        $response = $this->httpClient->request('GET', $this->serverUrl.'/delivery/schemas.json',
            [ 'headers' => ['cookie' => 'token='.$this->authToken] ]
        );
        return $response;
    }

    public function placeProposal(){
        $name = 'string';
        $surname = 'string';
        $patronymic = 'string';
        $phone = 'string';
        $addr = 'string';
        $lat = 'string';
        $lon = 'string';
        $contact_name = 'string';
        $contact_phone = 'string';
        $addr = 'string';
        $lat = 'string';
        $lon = 'string';
        $begin = 'string';
        $end = 'string';
        $code = 'string';
        $name = 'string';
        $count = 'string';
        $cost = 'string';
        $weight = 'string';
        $volume = 'string';
        $schema = 'string';
        $legal_entity = 'string';
        $pay_method = 'string';
        $external_id = 'string';
        $tail_lift = 'string';
        $loaders = 'string';
        $body = [
            'name' => $name,  // - имя покупателя
            'surname' => $surname,  // - фамилия (необязательный)
            'patronymic' => $patronymic,  // - отчество (необязательный)
            'phone' => $phone,  // - телефон покупателя в формате ^7[0-9]{10}$
            'from' => [
              'addr' => $addr,  // - адрес склада
              'lat' => $lat,  // - широта склада (необязательный)
              'lon' => $lon,  // - долгота склада (необязательный)
              'contact_name' => $contact_name,  // - имя контактного лица на складе
              'contact_phone' => $contact_phone,  // - контактный телефон на складе
            ],
            'to' => [
              'addr' => $addr,  // - адрес доставки
              'lat' => $lat,  // - широта доставки (необязательный)
              'lon' => $lon,  // - долгота доставки (необязательный)
            ],
            'time_slot' => [
              'begin' => $begin,  // - начальный интервал доставки в формате 2017-03-23T11:46:15+0300
              'end' => $end,  // - начальный интервал доставки в формате 2017-03-23T14:46:15+0300
            ],
            'products' => [
                1 => [
                    'code' => $code,  // - артикул товара
                    'name' => $name,  // - название товара
                    'count' => $count,  // - количество единиц товара
                    'cost' => $cost,  // - стоимость товара
                    'weight' => $weight,  // - вес в кг
                    'volume' => $volume,  // - объём м³
                  ]
            ],
            'schema' => $schema,  // - схема доставки (id)
            'legal_entity' => $legal_entity,  // - юр. лицо (id)
            'pay_method' => $pay_method,  // - способ оплаты (cash, card, bank)
            'external_id' => $external_id,  // - внешний id заявки (необязательный)
            'requirements' => [
                'tail_lift' => $tail_lift,  // - 1, если требуется гидроборт
                'loaders' => $loaders,  // - необходимое количество грузчиков
            ]
        ];
        $response = $this->httpClient->request('POST', $this->serverUrl.'/delivery.json',
            [ 'headers' => ['cookie' => 'token='.$this->authToken], 'body' => $body ]
        );
        return $response;
    }

    public function calculateProposal(){}

    public function getStatus(){}

    public function changeProposalStatus(){}

    public function getProposalPerformer(){}

    public function getProposalSubscribeScript($cb){}

}
