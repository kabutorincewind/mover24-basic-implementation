<?php

namespace App\Controller;

use App\Services\MoverApi;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="index`")
     */
    public function index(MoverApi $moverApi)
    {
      $response = $moverApi->auth();
      // $response = $moverApi->getSchemas();
      return new JsonResponse($response->getContent().' token='.$moverApi->authToken, $response->getStatusCode() /*$headers*/);
    }
}
